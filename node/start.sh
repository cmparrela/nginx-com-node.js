#!/bin/bash

npm install

dockerize -wait tcp://desafio.database:3306 -timeout 25s
npx knex migrate:latest

npm start