const config = require('./knexfile.js')['development'];
const knex = require('knex')(config);


const express = require('express');
const app = express();

app.get('/', async (req, res) => {
    let saida = '<h1>Full Cycle Rocks!</h1>'
    const peoples = await knex.select("*").from('people');
    peoples.forEach((people) => {
        saida = saida.concat(`<h4>${people.name}</h4>`);
    });

    res.send(saida)
})
app.listen(3000, () => console.log('Server is up and running'));