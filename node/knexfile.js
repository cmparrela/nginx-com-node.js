module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host: 'desafio.database',
      user: 'root',
      password: 'root',
      database: 'desafio'
    },
    migrations: {
      tableName: 'migrations'
    }
  }
};
